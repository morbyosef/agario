import pygame
from random import randint
import math
import socket


def random_color():

    red = randint(0,240)
    green = randint(0, 240)
    blue = randint(0, 240)
    return (red, green, blue)

def random_location():
    x = randint(0,500)
    y = randint(0,500)
    return [x,y]


"""class Enemy:
    color = random_color()
    size = 20
    location = [100, 200]
    speed = 2

    def move_towards_player(self, player):
        dx, dy = self.location[0] - player.location[0] , self.location[1] - player.location[1]
        dist = int(math.hypot(dx, dy))
        dx, dy = int(dx / dist), int(dy / dist)
        self.location[0] += dx * self.speed
        self.location[1] += dy * self.speed"""


class Player:
    def __init__(self, color = None, size = None, location = None, speed = None):
        if color is None:
            self.color = ()
        else:
            self.color = color
        if size is None:
            self.size = 0
        else:
            self.size = size
        if location is None:
            self.location = []
        else:
            self.location = location
        if speed is None:
            self.speed = speed
        else:
            speed = 0


    def collision(self, enemyList):
        for enemyC in enemyList:
            if (enemyC.location[0] - self.location[0])**2 + (enemyC.location[1] - self.location[1])**2 <= (self.size + enemyC.size)**2:
                return True
        return False


# #########
# ####################GAME###############################
# #########

# Import socket module


# Create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Define the port on which you want to connect
port = 7777

# connect to the server on local computer
s.connect(('10.10.0.253', port))
pygame.init()

size = width, height = 500,500
screen = pygame.display.set_mode(size)

clock = pygame.time.Clock()

screen.fill((255, 255, 255))

running = True

while running:
    clock.tick(120)
    pygame.display.flip()
    screen.fill((255, 255, 255))

    data = s.recv(1024)
    print(data)
    players = [Player(), Player()]

    pygame.draw.circle(screen, players[0].color, players[0].location, players[0].size)
    pygame.draw.circle(screen, players[1].color, players[1].location,players[1].size)
    # if enemy1.location[0] != 0 and enemy1.location[0] != 500 and enemy1.location[1] != 0 and enemy1.location[1] != 500:
    #    enemy1.move_towards_player(player1)

    keys = pygame.key.get_pressed()

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            s.sendall(-1)

        if event.type == pygame.KEYDOWN:
            """
            if event.key == pygame.K_LEFT and player1.location[0] != 0:
                player1.location[0] -= 1
            if event.key == pygame.K_RIGHT and player1.location[0] != 300:
                player1.location[0] += 1
            if event.key == pygame.K_UP and player1.location[1] != 0:
                player1.location[1] -= 1
            if event.key == pygame.K_DOWN and player1.location[1] != 300:
                player1.location[1] += 1"""
    if keys[pygame.K_UP] and players[0].location[1] != 0 :
        players[0].location[1] -= players[0].speed
    if keys[pygame.K_DOWN] and players[0].location[1] !=500:
        players[0].location[1] += players[0].speed
    if keys[pygame.K_RIGHT] and players[0].location[0] != 500:
        players[0].location[0] += players[0].speed
    if keys[pygame.K_LEFT] and players[0].location[0] != 0:
        players[0].location[0] -=players[0].speed
    if players[0].collision(players[1]):
        if players[1].size > players[0].size:
            s.sendall(players[1])
            running = False
        if players[0].size > players[1].size:
            players[0].size +=  players[1].size
    # send new data

pygame.quit()