import socket
import json
from random import randint
import threading

def random_color():

    red = randint(0,240)
    green = randint(0, 240)
    blue = randint(0, 240)
    return (red, green, blue)

serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

host = "10.10.0.253"
port = 7777

serverSocket.bind((host, port))
playersList = []
serverSocket.listen(2)

def generatePlayersJson():
    playersArr = []
    for playerObject in playersList:
        playerJson = {
            "id": playerJson.id,
            "location": playerObject.loc,
            "size": playerObject.size,
            "color": playerObject.color
        }
        playersArr.append(playerJson)
    return playersArr
        

def clientHandler(clientSocket, clientAddr):
    print("Client connected from: " + str(clientaddr))

    clientSocket.send(b"1")
    while True:
        data = clientSocket.recv(1024)
        print(data)
        pass

print("Listening")
while True:
    clientsocket, clientaddr = serverSocket.accept()
    print("connected")
    playersList.append(clientaddr)
    handler = threading.Thread(target=clientHandler,args = (clientsocket, clientaddr))
    handler.start()

class player:
    location = []
    size = 10
    color = ((0, 0, 0))
    def __init__(self, id, color, loc):
        self.id = id 
        self.color = color
        self.location = loc
